/*
 *  vector.cpp
 *  SparseMatrix
 *
 *  Created by Brad Marston on 4/1/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */


#import <libc.h>

#import "vector.h"

extern long Nx;
extern double Lx;
extern double dx;
extern double Ly;
extern double dy;
extern double Lz;
extern double dz;


const double reciprocolLarge = 1.0/((double)(pow(2, 31) - 1));
double ran(void) // random number between 0 and 1
{
    return reciprocolLarge * random();
}

void random(vector *a)
{
/*    for (long i = 0; i < a->d; i++) {
        a->component[i] = ran() - 0.5;
    }*/
    

    for (long i = 1; i < Nx-1; i++) {
        for (long j = 1; j < Nx-1; j++) {
            for (long k = 1; k < Nx-1; k++) {
                double x = -0.5*Lx+dx*i;
                double y = -0.5*Ly+dy*j;
                double z = -0.5*Lz+dz*k;
                a->component[i*Nx*Nx+j*Nx+k] = exp(2.*(-x*x/Lx-y*y/Ly-z*z/Lz));
            }
        }
    }
    
    normalize(a);
}

void multiplyAdd(const vector *a, const vector *b, const double c, vector *d)
{
    /* d->eigenvalue = a->eigenvalue;
    
    for (long i = 0; i < a->d; i++) {
        d->component[i] = a->component[i] + c * b->component[i];
    } */
    
    d->eigenvalue = a->eigenvalue;
    memcpy(d->component, a->component, a->d*sizeof(double));
    
    cblas_daxpy(a->d, c, b->component, 1, d->component, 1);
}
