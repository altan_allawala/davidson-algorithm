		/* DAVIDSON.C: low lying eigenvectors and values */
	      /* of REAL sparse, Hermitian Hamiltonian */
	 	   /* using the Davidson-Liu algorithm */

/*
	see:  Ernest R. Davidson, J. Comp. Phys. 17, 87 -- 94 (1975) 	
	      and Comp. in Phys. 7, 519 -- 522 (1993);
	      Ronald B. Morgan, J. Comp. Phys. 101, 287 -- 291 (1992);
	      and Albert Booten and Henk van der Vorst, Comp. in Phys. 10,
	      239 -- 242 and 331 -- 334 (1996).
 
 
        Created by Brad Marston on 4/14/06.
        Modified & Optimized by Altan Allawala.
 
        Currently the operator Ldagger L, where L is the Fokker-Planck operator for the Lorenz attractor
 
*/

extern long Nx;
extern double Lx;
extern double dx;
extern double Ly;
extern double dy;
extern double Lz;
extern double dz;

double sigma = 3.;//10.;
double beta = 0.16;//1.;//8./3.;
double rho = 26.5;//28.;
double gama = 1.;//0.05;

#import <dispatch/dispatch.h>

//#import <complex>
//typedef std::complex<double> complex;

#import "davidson.h"

bool davidson(vector v[DIM], vector Av[DIM], vector *q, const long k, const vector eigenvector[])  
{	
    long m = 1; /* initial dimension of subspace */

    vector *Avv = new vector;
    /*vector *diagonals = new vector;
    
     for (long i = 1; i < Nx-1; i++) {
        for (long j = 1; j < Nx-1; j++) {
            for (long k = 1; k < Nx-1; k++) {
     
                double x = -0.5*Lx+dx*i;
                double y = -0.5*Ly+dy*j;
                double z = -0.5*Lz+dz*k;
     
                diagonals[i*Nx*Nx+j*Nx+k] = (0.5*oneOverDx2) * (sigma*sigma*(x-y)*(x-y) + (rho*x-x*z-y)*(rho*x-x*z-y) + (x*y-beta*z)*(x*y-beta*z)) + (1.+sigma+beta - 6*gama*oneOverDx2)*(1.+sigma+beta - 6*gama*oneOverDx2) + 6.*gama*gama*oneOverDx2*oneOverDx2;
            }
        }
     }
     */
    
    /* orthonormalize starting vector w.r.t. eigenvectors already found */
    orthonormalize(&v[0], k, eigenvector);
    
    /* (A) compute matrix elements A in reduced basis and diagonalize */
    LinearOperator(&v[0], &Av[0], Avv);
        
    /* initial eigenvalue and eigenvector */
    double lambda[DIM];
    memset(lambda, 0, DIM*sizeof(double));
    double alpha[DIM*DIM];
    memset(alpha, 0, DIM*DIM*sizeof(double));
    
    double *vAv = new double[DIM*DIM];
    vAv[0] = dot(&v[0], &Av[0]);
    lambda[0] = vAv[0];
    alpha[0] = 1.0;
    
    
    /* begin iterating: initial values of control parameters */
    double qnorm = 1.0; /* large initial value */

    
    /* iteration loop */
    while ((qnorm > DISCREPANCY) && (m < DIM)) {
        
        /* (B) form q */
        (*q).zero(); 
        for (long i = 0; i < m; i++) {
            multiplyAdd(q, &Av[i], alpha[i], q);
            multiplyAdd(q, &v[i], -alpha[i] * lambda[0], q);
        }
        
        /* (C) calculate and report error */
        qnorm = sqrt(dot(q, q));

        /* (D) form v[m] using preconditioner */
        preconditioner(lambda[0], q, &v[m]);
        
        /* (E1) orthogonalize v[m] w.r.t. existing v's */
        orthonormalize(&v[m], m, v);
        
        /* (E2) orthonormalize v[m] w.r.t. eigenvectors already found */
        orthonormalize(&v[m], k, eigenvector);
            
        /* (F) add new row and column to matrix A */
        LinearOperator(&v[m], &Av[m], Avv);
        void (^block1)(size_t i) = ^(size_t i) {
            vAv[i*DIM+m] = dot(&v[i], &Av[m]);
        };
        
        dispatch_queue_t globalQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_apply(m+1, globalQueue, block1);

        m = m + 1; /* increase size of subspace */
        
        /* (G) diagonalize A and return to step (B) */
        double a[DIM*DIM];
        for (long i = 0; i < m; i++) { 
            for (long j = i; j < m; j++) {
                a[i+(j+1)*j/2] = vAv[i*DIM+j];
            }
        }
        
        char jobz, uplow;
        jobz = 'v';
        uplow = 'u';
//        complex work[2*DIM];
//        double rwork[3*DIM];
        double work[3*DIM];
        int info;        
        int status = dspev_(&jobz, &uplow, (__CLPK_integer *)(&m), (__CLPK_doublereal *)a, lambda,
                            (__CLPK_doublereal *)alpha, (__CLPK_integer *)(&m), (__CLPK_doublereal *)work, (__CLPK_integer *)(&info));

        if (info != 0) {
            std::cerr << "dspev failed.  status = " << status << "\n";
            exit(EXIT_FAILURE);
        }        
    }
    
    delete Avv;
    delete[] vAv;
    
    std::cout << "\n\nsize \t\teigenvalue \t\terror\n";
    std::cout << m << "\t\t" << lambda[0] << "\t\t" << qnorm << "\n\n";
    std::cout.flush();
    
    
    /* update v[0] */
    (*q).zero();
    for (long i = 0; i < m; i++) {
        multiplyAdd(q, &v[i], alpha[i], q);
    }
    normalize(q);
    
    v[0] = *q;
    eigenvalue(&v[0], lambda[0]);
        
    /* finished? */
    if (m == DIM) return false;
    else return true;
}

void orthonormalize(vector *x, const long k, const vector v[])
{
    double c;
    for (long i = 0; i < k; i++) {
        c = dot(&v[i], x);
        multiplyAdd(x, &v[i], -c, x);
    }
    normalize(x);

}

void LinearOperator(const vector *v, vector *Av, vector *Avv)
{
    long Nx2 = Nx*Nx;
    double oneOver2dx = 1.0/(2.0*dx);
    double oneOverDx2 = 1.0/(dx*dx);
    double oneOver2dy = 1.0/(2.0*dy);
    double oneOverDy2 = 1.0/(dy*dy);
    double oneOver2dz = 1.0/(2.0*dz);
    double oneOverDz2 = 1.0/(dz*dz);

    void (^block1)(size_t i) = ^(size_t i) {
        for (long j = 1; j < Nx-1; j++) {
            for (long k = 1; k < Nx-1; k++) {
                
                int n = (i+1)*Nx2+j*Nx+k;
                int m = (Nx-2-i)*Nx2+(Nx-1-j)*Nx+k;
                
                double x = -0.5*Lx+dx*(i+1);
                double y = -0.5*Ly+dy*j;
                double z = -0.5*Lz+dz*k+25.;
                
                Avv->component[n] = -(sigma*(y-x-dx)*v->component[n+Nx2] - sigma*(y-x+dx)*v->component[n-Nx2])*oneOver2dx;
                Avv->component[n] += -((rho*x-x*z-y-dy)*v->component[n+Nx] - (rho*x-x*z-y+dy)*v->component[n-Nx])*oneOver2dy;
                Avv->component[n] += -((x*y-beta*(z+dz))*v->component[n+1] - (x*y-beta*(z-dz))*v->component[n-1])*oneOver2dz;
                Avv->component[n] += gama*(v->component[n+Nx2] + v->component[n-Nx2] - 2.*v->component[n])*oneOverDx2;
                Avv->component[n] += gama*(v->component[n+Nx] + v->component[n-Nx] - 2.*v->component[n])*oneOverDy2;
                Avv->component[n] += gama*(v->component[n+1] + v->component[n-1] - 2.*v->component[n])*oneOverDz2;
                Avv->component[m] = Avv->component[n];
                
            }
        }
    };

    dispatch_queue_t globalQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_apply(Nx/2-1, globalQueue, block1);

/*
    for (long i = 1; i < Nx/2; i++) {
        for (long j = 1; j < Nx-1; j++) {
            for (long k = 1; k < Nx-1; k++) {
                
                n = i*Nx2+j*Nx+k;
                m = (Nx-1-i)*Nx2+(Nx-1-j)*Nx+k;
                
                x = -0.5*Lx+dx*i;
                y = -0.5*Ly+dy*j;
                z = -0.5*Lz+dz*k+23.;
                
                DxVxP = (sigma*(y-x-dx)*v->component[n+Nx2] - sigma*(y-x+dx)*v->component[n-Nx2])*oneOver2dx;
                DyVyP = ((rho*x-x*z-y-dy)*v->component[n+Nx] - (rho*x-x*z-y+dy)*v->component[n-Nx])*oneOver2dy;
                DzVzP = ((x*y-beta*(z+dz))*v->component[n+1] - (x*y-beta*(z-dz))*v->component[n-1])*oneOver2dz;
                
                Dx2 = (v->component[n+Nx2] + v->component[n-Nx2] - 2.*v->component[n])*oneOverDx2;
                Dy2 = (v->component[n+Nx] + v->component[n-Nx] - 2.*v->component[n])*oneOverDy2;
                Dz2 = (v->component[n+1] + v->component[n-1] - 2.*v->component[n])*oneOverDz2;
                
                Avv->component[n] = -DxVxP - DyVyP -DzVzP + gama*(Dx2+Dy2+Dz2);
                Avv->component[m] = Avv->component[n];
                
            }
        }
    }
*/
 
    void (^block2)(size_t i) = ^(size_t i) {
        for (long j = 1; j < Nx-1; j++) {
            for (long k = 1; k < Nx-1; k++) {
                
                long n = (i+1)*Nx2+j*Nx+k;
                long m = (Nx-2-i)*Nx2+(Nx-1-j)*Nx+k;
                
                double x = -0.5*Lx+dx*(i+1);
                double y = -0.5*Ly+dy*j;
                double z = -0.5*Lz+dz*k+25.;
                
                 Av->component[n] = sigma*(y-x)*(Avv->component[n+Nx2] - Avv->component[n-Nx2])*oneOver2dx;
                 Av->component[n] += (rho*x-x*z-y)*(Avv->component[n+Nx] - Avv->component[n-Nx])*oneOver2dy;
                 Av->component[n] += (x*y-beta*z)*(Avv->component[n+1] - Avv->component[n-1])*oneOver2dz;
                 Av->component[n] += gama*(Avv->component[n+Nx2] + Avv->component[n-Nx2] - 2.0*Avv->component[n])*oneOverDx2;
                 Av->component[n] += gama*(Avv->component[n+Nx] + Avv->component[n-Nx] - 2.0*Avv->component[n])*oneOverDy2;
                 Av->component[n] += gama*(Avv->component[n+1] + Avv->component[n-1] - 2.0*Avv->component[n])*oneOverDz2;
                 Av->component[m] = Av->component[n];

            }
        }
    };
    
    dispatch_apply(Nx/2-1, globalQueue, block2);

    Avv->zero();

}

void preconditioner(double lambda, const vector *q, vector *v)
{
    double oneOverDx2 = 1.0/(dx*dx);
    double oneOverDy2 = 1.0/(dy*dy);
    double oneOverDz2 = 1.0/(dz*dz);
    
    double diagonal;
    
    double precond;
//    for (long i = 0; i < q->d; i++) {
    for (long i = 1; i < Nx-1; i++) {
        for (long j = 1; j < Nx-1; j++) {
            for (long k = 1; k < Nx-1; k++) {
                    // NEW PRECONDITIONER
                double x = -0.5*Lx+dx*i;
                double y = -0.5*Ly+dy*j;
                double z = -0.5*Lz+dz*k+25.;
                diagonal = 0.5 * (oneOverDx2*sigma*sigma*(x-y)*(x-y) + oneOverDy2*(rho*x-x*z-y)*(rho*x-x*z-y) + oneOverDz2*(x*y-beta*z)*(x*y-beta*z)) + gama*gama*(6.*(oneOverDx2*oneOverDx2+oneOverDy2*oneOverDy2+oneOverDz2*oneOverDz2)+8.*(oneOverDx2*oneOverDy2+oneOverDx2*oneOverDz2+oneOverDy2*oneOverDz2));
                //diagonal = 1.;
                precond = fabs(diagonal - lambda) + EPSILON;
                int n = i*Nx*Nx+j*Nx+k;
                v->component[n] = q->component[n] / precond;
            }
        }
    }
    v->eigenvalue = lambda;
}   

