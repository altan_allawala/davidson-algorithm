# README #

This code, originally written by my advisor Prof. Brad Marston, computes the ground state eigenvalues (and optionally, eigenvectors) of any Hermitian operator using the Davidson algorithm. This code required a dense symmetric matrix as an input. I modified it to compute the eigenvalues/eigenvectors of any Hermitian operator, in the process reducing the required number of operations (making it analogous to a sparse matrix problem).

Currently the operator is set to be L^H L where L is the Fokker-Planck operator of the Lorenz attactor, given by:

L P = -D(v P) + g * D(D(P)),

where v is the equations of motion of the dynamics (of the Lorenz attractor) and L^H is the Hermitian transpose of L.

By expressing the Fokker-Planck problem as L^H L, the operator becomes self-adjoint, making it's ground-state amenable to being solved by the Davidson algorithm.

This code was used for some results in the following publication:
http://export.arxiv.org/abs/1604.00867