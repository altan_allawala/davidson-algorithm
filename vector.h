/*
 *  vector.h
 *  SparseMatrix
 *
 *  Created by Brad Marston on 4/1/06.
 *  Copyright 2006 __MyCompanyName__. All rights reserved.
 *
 */

#import <iostream>
#import <complex>
#import <Accelerate/Accelerate.h>

typedef std::complex<double> complex;

//class sparseMatrix;

double ran(void);

class vector {
    
    friend void eigenvalue(vector *a, double eigenvalue);
    friend double eigenvalue(vector *a);
    friend void normalize(vector *a);
    friend void random(vector *a);
    friend void multiplyAdd(const vector *a, const vector *b, const double c, vector *d);
    friend double dot(const vector *a, const vector *b);
    //friend double operator*(const vector &a, const vector &b);
    friend std::ostream &operator<<(std::ostream &os, vector *v);
    friend std::istream &operator>>(std::istream &is, vector *v);
    
    //friend void sparseMultiply(const sparseMatrix *h, const vector *v, vector *Av);
    //friend void preconditioner(const sparseMatrix *h, double lambda, const vector *q, vector *v);
    
    friend void LinearOperator(const vector *v, vector *Av, vector *Avv);
    friend void preconditioner(double lambda, const vector *q, vector *v);

public:
    static long d;    
    vector(); // the default constructor
    ~vector();
    void zero();
    vector &operator=(const vector &v);
    
private:
    double eigenvalue;
    double *component;

};

inline vector::vector()
{
    eigenvalue = 0.0;
    component = (double *)calloc(d, sizeof(double));
}

inline vector::~vector()
{
    if (component) free(component); 
}   

inline void vector::zero()
{
    eigenvalue = 0.0;
    bzero(component, d*sizeof(double));
}

inline vector &vector::operator=(const vector &v)
{
    eigenvalue = (&v)->eigenvalue;
    memcpy(component, (&v)->component, d*sizeof(double));
    return *this;
}

inline void eigenvalue(vector *a, double eigenvalue)
{
    a->eigenvalue = eigenvalue;
}

inline double eigenvalue(vector *a)
{
    return a->eigenvalue;
}

inline void normalize(vector *a)
{
    double inner = dot(a, a);
    double norm = pow(inner, -0.5); //also changed this
    long i;
    for (i = 0; i < a->d; i++) {
        a->component[i] = norm * a->component[i];        
    }
}
/*
inline double operator*(const vector &a, const vector &b)
{
    long i;
    double inner; //also changed this
    inner = 0.0;
    for (i = 0; i < a.d; i++) {
        inner += a.component[i] * b.component[i];// conj(a.component[i]) * b.component[i];
    }
    
    return inner;
}
*/
inline double dot(const vector *a, const vector *b)
{
    return cblas_ddot(a->d, a->component, 1, b->component, 1);
}

inline std::ostream &operator<<(std::ostream &os, vector *v)
{
    os << v->d << "\t\t" << v->eigenvalue << "\n";
    for (long i = 0; i < v->d; i++) {
        os << v->component[i] << "\n";// << "\t" << 0. << "\n";
    }
    
    return os;
}

inline std::istream &operator>>(std::istream &is, vector *v)
{
    while (is.peek() == '%') {
        is.ignore(1024, '\n');
    }    
    
    is >> v->d >> v->eigenvalue;
    
    double real;
//    double real, imag;
    for (long i = 0; i < v->d; i++) {
        if (is >> real) {
//        if (is >> real >> imag) {
            v->component[i] = real;//complex(real, imag);
        } else {
            std::cerr << "Mismatch in number of vector components";
            exit(EXIT_FAILURE);
        }
    }
        
    return is;
}

